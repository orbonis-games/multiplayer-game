import { EntityData, SyncRequest, SyncResponse } from "shared/src/sync";
import io from "socket.io-client";

export type SyncCallback = (response: SyncResponse) => void;
export type SyncRequestCallback = () => SyncRequest;
export type SyncDisconnected = (id: string) => void;

class Socket {
    private readonly SYNC_INTERVAL: number = 1000 / 30;

    public onSync: SyncCallback[] = [];
    public onRegister: SyncCallback[] = [];
    public onDisconnected: SyncDisconnected[] = [];

    private io?: io.Socket;
    private syncing?: SyncCallback;
    private lastSync: number = Date.now();

    private id: string = "";

    public getID(): string {
        return this.id;
    }

    public connect(): Promise<void> {
        return new Promise((resolve) => {
            this.io = io.io();

            this.io.on("connect", () => resolve());

            this.io.on("sync", (response: SyncResponse) => {
                console.debug("Synced", response);
                if (this.syncing !== undefined) {
                    this.syncing(response);
                }
                this.syncing = undefined;
                this.lastSync = Date.now();
                this.onSync.forEach((x) => x(response));
            });

            this.io.on("disconnected", (id: string) => {
                this.onDisconnected.forEach((x) => x(id));
            });
        });
    }

    public register(id?: string): Promise<EntityData> {
        return new Promise((resolve) => {
            if (this.io) {
                this.io.once("register", (id: string, data: EntityData) => {
                    this.id = id;
                    resolve(data);
                });
                this.io.emit("register", id);
            } else {
                throw new Error("Attempted to register without io connection.");
            }
        });
    }

    public ping(): Promise<void> {
        return new Promise((resolve) => {
            console.debug("ping");
    
            this.io?.once("pong", () => {
                console.debug("pong");
                resolve();
            });
    
            this.io?.emit("ping");
        });
    }

    public sync(request: SyncRequest): Promise<SyncResponse> {
        return new Promise((resolve: SyncCallback) => {
            if (this.syncing === undefined) {
                this.syncing = resolve;
                console.debug("Syncing...");
                this.io?.emit("sync", request);
            } else {
                throw new Error("Sync already in progress. Wait for the previous sync before starting a new one.");
            }
        });
    }

    public continuousSync(getRequest: SyncRequestCallback): void {
        window.setTimeout(() => {
            this.sync(getRequest()).then(() => this.continuousSync(getRequest));
        }, this.SYNC_INTERVAL / (Date.now() - this.lastSync));
    }

    public spawnZombie(): Promise<void> {
        return new Promise((resolve) => {
            this.io?.emit("zombie", resolve);
        });
    }
}

export const socket: Socket = new Socket();
