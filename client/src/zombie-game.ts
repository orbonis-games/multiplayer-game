import { Game } from "libs/core-game/src/game/game";
import { Entity } from "./game/entity";
import { EntityManager } from "./game/entity-manager";
import { socket } from "./server/socket";

export class ZombieGame extends Game {
    protected async initRenderables(): Promise<void> {
        const pawns: EntityManager = new EntityManager("entity-manager", this.app!);
        pawns.onEntityChange.push((...entities) => this.onEntityChange(...entities));
        
        await socket.connect();
        await socket.ping();

        await super.initRenderables(
            pawns
        );

        socket.continuousSync(() => {
            return {
                player: pawns.getPlayer().getData()
            };
        });
    }

    protected onEntityChange(...entities: Entity[]): void {
        this.getCollision().removeAll((x) => x instanceof Entity);
        this.getCollision().add(...entities);
    }
}