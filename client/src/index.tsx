import React from "react";
import ReactDOM from "react-dom";

import "libs/core-game/src/core.scss";
import "libs/core-game/src/utils/contextual-css.scss";
import "client/src/style.scss";

import "@pixi/graphics-extras";

import { setupContextualCSS } from "libs/core-game/src/utils/contextual-css";
import { Application } from "libs/core-game/src/application";
import { ZombieGame } from "./zombie-game";

const element = document.createElement("div");
element.id = "app";
document.body.appendChild(element);

setupContextualCSS();

ReactDOM.render(
    <Application
        bitbucket=""
        audioCredit=""
        game={ZombieGame}
    />
, element);