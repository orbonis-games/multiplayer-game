import { EquippedWeapon, Weapon } from "./weapon";

const MAX_WEAPON_COUNT = 3;
export class WeaponManager {

    protected currentWeapons: EquippedWeapon[];
    protected currentIndex: number;

    constructor(weapons: Weapon[]) {
        this.currentIndex = 0;
        this.currentWeapons = weapons as EquippedWeapon[];
    }

    public fireWeapon(index: number): boolean {
        if (this.currentWeapons[index].clipCount > 0 && this.currentWeapons[index].clipSize > 0) {
            this.currentWeapons[index].remainingBullets -= 1;
            return true;
        }
        return false;
    }

    public reloadWeapon(index: number): boolean {
        const weapon = this.currentWeapons[index];
        if (weapon.remainingClips > 0) {
            weapon.remainingClips -= 1;
            weapon.remainingBullets = weapon.clipSize;
            this.currentWeapons[index] = weapon;
            return true;
        }
        return false;
    }

    public removeWeapon(index: number): boolean {
        if (this.currentWeapons.length > index) {
            this.currentWeapons.splice(index, 1);
            this.currentIndex = Math.min(this.currentWeapons.length - 1, this.currentIndex);
            return true;
        }
        return false;
    }

    public changeWeapon(index: number): boolean {
        if (index < this.currentWeapons.length) {
            this.currentIndex = index;
            return true;
        }
        return false;
    }

    public equipNewWeapon(newWeapon: Weapon): void {
        if (this.currentWeapons.length < MAX_WEAPON_COUNT) {
            this.currentWeapons.push(newWeapon as EquippedWeapon);
            this.currentIndex = this.currentWeapons.length - 1;

        } else {
            this.currentWeapons[this.currentIndex] = newWeapon as EquippedWeapon;
        }
        const currentWeapon = this.currentWeapons[this.currentIndex];
        currentWeapon.remainingBullets = currentWeapon.clipSize;
        currentWeapon.remainingClips = currentWeapon.clipCount;
        this.currentWeapons[this.currentIndex] = currentWeapon;
    }

    protected getCurrentWeapon(): Weapon {
        return this.currentWeapons[this.currentIndex];
    }

}