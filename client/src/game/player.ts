import { EntityData } from "shared/src/sync";
import { Entity } from "./entity";

export class Player extends Entity {
    public async init(data?: EntityData): Promise<void> {
        await super.init(data);
        
        if (!this.remote) {
            this.setupControls();
        }
    }

    public render(delta: number): void {
        super.render(delta);
    }

    protected updateGraphic(): void {
        super.updateGraphic();

        if (this.data && this.label) {
            this.label.text = this.data.name;
        }
    }

    protected setupControls(): void {
        window.addEventListener("keydown", (e) => {
            switch (e.key) {
                case "ArrowUp":
                    this.moveDelta.y = -1;
                    break;
                case "ArrowDown":
                    this.moveDelta.y = 1;
                    break;
                case "ArrowLeft":
                    this.moveDelta.x = -1;
                    break;
                case "ArrowRight":
                    this.moveDelta.x = 1;
                    break;
                case "Shift":
                    this.speedMod = 2;
                    break;
            }
        });

        window.addEventListener("keyup", (e) => {
            switch (e.key) {
                case "ArrowUp":
                    if (this.moveDelta.y < 0) {
                        this.moveDelta.y = 0;
                    }
                    break;
                case "ArrowDown":
                    if (this.moveDelta.y > 0) {
                        this.moveDelta.y = 0;
                    }
                    break;
                case "ArrowLeft":
                    if (this.moveDelta.x < 0) {
                        this.moveDelta.x = 0;
                    }
                    break;
                case "ArrowRight":
                    if (this.moveDelta.x > 0) {
                        this.moveDelta.x = 0;
                    }
                    break;
                case "Shift":
                    this.speedMod = 1;
                    break;
            }
        });
    }
}