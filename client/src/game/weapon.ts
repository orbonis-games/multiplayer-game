export interface Weapon {
    fireRate: number; // bullets per second
    bulletDamage: number;
    clipSize: number;
    clipCount: number;
    reloadTime: number;
    burstAmount: number
    isAuto: boolean;
}

export interface EquippedWeapon extends Weapon {
    remainingClips: number;
    remainingBullets: number;
}

export function GetPistol(): Weapon {
    const pistol: Weapon = { fireRate: 2, bulletDamage: 1, clipSize: 7, clipCount: 6, reloadTime: 0.5, burstAmount: 1, isAuto: false };
    return pistol;
}

export function GetHuntingRifle(): Weapon {
    const huntingRifle: Weapon = { fireRate: 0.5, bulletDamage: 5, clipSize: 1, clipCount: 15, reloadTime: 2, burstAmount: 1, isAuto: false };
    return huntingRifle;
}

export function GetAssaultRifle(): Weapon {
    const assaultRifle: Weapon = { fireRate: 5, bulletDamage: 0.6, clipSize: 30, clipCount: 3, reloadTime: 1.2, burstAmount: 1, isAuto: true };
    return assaultRifle;
}

export function GetBurstRifle(): Weapon {
    const burstRifle: Weapon = { fireRate: 2, bulletDamage: 0.8, clipSize: 12, clipCount: 8, reloadTime: 1, burstAmount: 3, isAuto: false };
    return burstRifle;
}