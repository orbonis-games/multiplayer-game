import { Application, Circle, Container, Graphics, Point, Text, TextStyle } from "pixi.js";
import { CollidableShape } from "libs/core-game/src/game/collision";
import { Renderable } from "libs/core-game/src/game/renderable";
import { EntityData } from "shared/src/sync";

export class Entity extends Renderable {
    protected readonly speed: number = 30;
    protected container?: Container;
    protected graphic?: Graphics;
    protected label?: Text;
    protected previousPosition: Point = new Point(0, 0);
    protected moveDelta: Point = new Point(0, 0);
    protected speedMod: number = 1;
    
    protected data?: EntityData;

    constructor(id: string, app: Application, protected remote: boolean) {
        super(id, app);
    }

    public init(data?: EntityData): Promise<void> {
        this.data = data;
        if (this.data) {
            this.container = new Container();
            this.updateGraphic();
            this.container.addChild(this.graphic!);
            this.app.stage.addChild(this.container, this.label!);
            this.container.position.set(this.data.position.x, this.data.position.y);
            return new Promise((resolve) => resolve());
        } else {
            throw new Error("Entity data should always be passed into init of Entity.");
        }
    }

    public render(delta: number): void {
        if (this.container) {
            if (this.colliding?.startsWith("zombie") || this.colliding?.startsWith("player")) {
                this.container.x = this.previousPosition.x;
                this.container.y = this.previousPosition.y;
            } else if (this.colliding === undefined) {
                this.previousPosition = this.container.position.clone();
                this.container.x += delta * this.moveDelta.x * (this.speed * this.speedMod);
                this.container.y += delta * this.moveDelta.y * (this.speed * this.speedMod);

                const diff: Point = new Point(
                    this.container.x - this.previousPosition.x,
                    this.container.y - this.previousPosition.y
                );

                if (diff.x !== 0 || diff.y !== 0) {
                    const rotation: number = Math.atan2(diff.y, diff.x);
                    this.container.rotation = rotation + Math.PI * 0.5;
                }
            }

            if (this.label) {
                this.label.x = this.container.x;
                this.label.y = this.container.y + 20;
            }

            if (this.data) {
                this.data.position.x = this.container.x;
                this.data.position.y = this.container.y;
                this.data.positionDelta.x = this.moveDelta.x;
                this.data.positionDelta.y = this.moveDelta.y;
            }
        }
    }

    public getColliders(): CollidableShape[] {
        if (this.container) {
            return [
                new Circle(this.container.x, this.container.y, 20)
            ];
        } else {
            return [];
        }
    }

    public isActiveCollider(): boolean {
        return true;
    }

    public isReady(): boolean {
        return this.data !== undefined;
    }

    public getData(): EntityData {
        if (this.data) {
            return this.data;
        } else {
            throw new Error("EntityData not set in Entity.");
        }
    }

    public setData(data: EntityData): void {
        this.data = data;
        if (this.container) {
            this.container.position = new Point(
                this.data.position.x,
                this.data.position.y
            );
            this.moveDelta = new Point(
                this.data.positionDelta.x,
                this.data.positionDelta.y
            );
        }
        this.updateGraphic();
    }

    public destroy(): void {
        this.container?.destroy();
        this.container = undefined;
    }

    protected updateGraphic(): void {
        if (!this.graphic) { 
            this.graphic = new Graphics();
        }
        if (!this.label) {
            this.label = new Text("", new TextStyle({
                align: "center",
                fontSize: 20
            }));
            this.label.anchor.set(0.5, 0);
            this.label.y = 20;
        }
        this.graphic.clear();
        if (this.data) {
            this.graphic.lineStyle(2, 0x222222, (this.remote) ? 0.5 : 1);
            this.graphic.beginFill(this.data.colour, (this.remote) ? 0.2 : 0.5);
            this.graphic.drawCircle(0, 0, 20);
            this.graphic.endFill();
            this.graphic.beginFill(0x222222, (this.remote) ? 0.5 : 1);
            this.graphic.moveTo(0, -25);
            this.graphic.lineTo(-5, -20);
            this.graphic.lineTo(5, -20);
            this.graphic.lineTo(0, -25);
            this.graphic.endFill();
        }
    }
}