import { CollidableShape } from "libs/core-game/src/game/collision";
import { Renderable } from "libs/core-game/src/game/renderable";
import { EntityData, SyncResponse } from "shared/src/sync";
import { socket } from "../server/socket";
import { Entity } from "./entity";
import queryString from "query-string";
import { Player } from "./player";
import { Zombie } from "./zombie";

export class EntityManager extends Renderable {
    public onEntityChange: Array<(...entities: Entity[]) => void> = [];

    private player?: Entity;
    private entities: { [key: string]: Entity } = {};

    public async init(): Promise<void> {
        this.player = new Player("player", this.app, false);
        this.entities[socket.getID()] = this.player;

        socket.onSync.push((response) => this.sync(response));
        socket.onDisconnected.push((id) => this.disconnected(id));
        
        const query: any = queryString.parse(location.search);
        const playerData: EntityData = await socket.register(query.id);
        await this.player.init(playerData);

        this.updateEntities();

        window.addEventListener("keyup", (e) => {
            if (e.key === " ") {
                socket.spawnZombie();
            }
        });
    }

    public render(delta: number): void {
        for (const key in this.entities) {
            this.entities[key].render(delta);
        }
    }

    public getColliders(): CollidableShape[] {
        return [];
    }

    public getPlayer(): Entity {
        if (this.player) {
            return this.player;
        } else {
            throw new Error("Player not set in Pawns.");
        }
    }

    private sync(response: SyncResponse): void {
        for (const key in response.players) {
            const isPlayer: boolean = (this.player?.isReady() ?? false) && key !== this.player?.getData().name;
            if (response.players[key].connected && isPlayer) {
                if (!(key in this.entities)) {
                    this.entities[key] = new Player(`player-${key}`, this.app, true);
                    this.entities[key].init(response.players[key]);
                } else {
                    this.entities[key].setData(response.players[key]);
                }
            } else if (key in this.entities) {
                this.entities[key].destroy();
                delete this.entities[key];
            }
        }

        for (const key in response.zombies) {
            if (response.zombies[key].connected) {
                if (!(key in this.entities)) {
                    this.entities[key] = new Zombie(`${key}`, this.app, true);
                    this.entities[key].init(response.zombies[key]);
                } else {
                    this.entities[key].setData(response.zombies[key]);
                }
            } else if (key in this.entities) {
                this.entities[key].destroy();
                delete this.entities[key];
            }
        }
        
        this.updateEntities();
    }

    private disconnected(id: string): void {
        this.entities[id]?.destroy();
        delete this.entities[id];
    }

    private updateEntities(): void {
        const entities: Entity[] = [];
        for (const key in this.entities) {
            entities.push(this.entities[key]);
        }
        this.onEntityChange.forEach((x) => x(...entities));
    }
}