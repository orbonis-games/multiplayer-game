import { Entity } from "./entity";

export class Zombie extends Entity {
    protected updateGraphic(): void {
        super.updateGraphic();

        if (this.label) {
            this.label.text = "Zombie";
        }
    }
}