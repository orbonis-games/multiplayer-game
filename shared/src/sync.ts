export interface EntityData {
    name: string;
    connected: boolean;
    position: { x: number, y: number };
    positionDelta: { x: number, y: number };
    colour: number;
}

export interface SyncRequest {
    player: EntityData;
}

export interface SyncResponse {
    players: { [key: string]: EntityData };
    zombies: { [key: string]: EntityData };
}
