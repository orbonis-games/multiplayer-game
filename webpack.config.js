const path = require("path");
const { compilerOptions } = require("./tsconfig-client.json");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const LiveReloadPlugin = require("@kooneko/livereload-webpack-plugin");

const data = {
    mode: "development",
    entry: "./client/src/index.tsx",
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "index.js"
    },
    devtool: "source-map",
    devServer: {
        contentBase: path.join(__dirname, "build"),
        port: 8080
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"],
        modules: [
            path.resolve(__dirname, 'node_modules'),
            path.resolve(__dirname, compilerOptions.baseUrl),
        ],
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                include: [
                    path.resolve(__dirname, "client", "src"),
                    path.resolve(__dirname, "shared", "src"),
                    path.resolve(__dirname, "libs", "core-game", "src")
                ],
                loader: "ts-loader",
                options: {
                    configFile: "tsconfig-client.json"
                }
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ],
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
                loader: require.resolve("url-loader"),
                options: {
                    limit: 10000,
                    name: "static/media/[name].[hash:8].[ext]",
                },
            },
            {
                test: [/\.eot$/, /\.ttf$/, /\.svg$/, /\.woff$/, /\.woff2$/],
                loader: require.resolve("file-loader"),
                options: {
                    name: "static/media/[name].[hash:8].[ext]",
                },
            }
        ]
    }
};

data.plugins = [
    new HTMLWebpackPlugin({
        title: "Multiplayer Game",
        template: "client/src/index.ejs",
        development: data.mode === "development"
    }),
    new CopyWebpackPlugin({
        patterns: [
            { from: "assets", to: "assets" }
        ]
    })
];

if (data.mode === "development") {
    data.plugins.push(new LiveReloadPlugin());
}

module.exports = data;