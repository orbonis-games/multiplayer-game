import express from "express";
import http, { Server } from "http";
import io from "socket.io";
import path from "path";
import { EntityData, SyncRequest, SyncResponse } from "shared/src/sync";
import { createBlankEntityData } from "./data";
import uniqid from "uniqid";

export class Application {
    private app: express.Application;
    private server: Server;
    private io: io.Server;

    private players: { [key: string]: EntityData } = {};
    private zombies: { [key: string]: EntityData } = {};

    constructor() {
        this.app = express();
        this.server = http.createServer(this.app);
        this.io = new io.Server(this.server);
    }

    public init(port: number): void {
        this.app.get('/', (req, res) => {
            res.sendFile(path.join(process.cwd(), "build", "index.html"));
        });
        
        this.app.get('*', (req, res) => {
            res.sendFile(path.join(process.cwd(), "build", req.path));
        });

        this.server.listen(port, () => {
            console.log("Server started");
        });
        
        this.io.on("connection", (socket: io.Socket) => {
            console.log(`Client ${socket.id} connected`);

            let playerID: string = uniqid();

            socket.on("register", (id?: string) => {
                playerID = id ?? playerID;
                console.log(`Client ${socket.id} registered as ${playerID}`);
                if (!(playerID in this.players)) {
                    this.players[playerID] = createBlankEntityData(playerID);
                }
                socket.emit("register", playerID, this.players[playerID]);
            });

            socket.on("disconnect", () => {
                console.log(`Client ${socket.id} disconnected`);
                if (playerID in this.players) {
                    this.players[playerID].connected = false;
                }
                this.io.emit("disconnected", playerID);
            });

            socket.on("ping", () => {
                socket.emit("pong");
            });

            socket.on("zombie", () => {
                const id: string = `zombie-${uniqid()}`;
                this.zombies[id] = createBlankEntityData(id);
                this.zombies[id].connected = true;
                this.zombies[id].colour = 0x88bf97;
                socket.emit("zombie");
                console.log(`Spawning ${id}`);
            });

            socket.on("sync", (request: SyncRequest) => this.sync(socket, playerID, request));
        });
    }

    private sync(socket: io.Socket, playerID?: string, request?: SyncRequest): void {
        if (playerID && request) {
            request.player.connected = true;
            this.players[playerID] = request.player;
        }

        const response: SyncResponse = {
            players: this.players,
            zombies: this.zombies
        };

        this.io.emit("sync", response);
    }
}
