import { EntityData } from "shared/src/sync";

export function createBlankEntityData(name: string): EntityData {
    return {
        name: name,
        connected: false,
        position: { x: 100 + (Math.random() * 800), y: 100 + (Math.random() * 800) },
        positionDelta: { x: 0, y: 0 },
        colour: Math.random() * 0xFFFFFF
    };
}